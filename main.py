import os
import sys
import time
import threading
import queue
import subprocess
from collections import defaultdict

from datetime import datetime
import logging

from enum import IntEnum, auto

import numpy as np

import cv2

from PyQt5.QtCore import QTimer, pyqtSignal, QObject
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QDialog, QApplication, QFileDialog, QMessageBox
from PyQt5.uic import loadUi


class LightState(IntEnum):
    OFF = 0
    ON = 1
    BLINK = 2


is_on_raw = defaultdict(bool)  # 色検出結果を格納する辞書型(デフォルトディクトとすること)
is_on_filtered = defaultdict(bool)  # チャタリング対策後の色検出状態（スレッド間で共有するのでロック掛けてアクセス）
blinking_state = defaultdict(LightState)

logger = logging.getLogger('test')
logger.setLevel(10)
fh = logging.FileHandler('test.log')
logger.addHandler(fh)


class StateMonitor():
    def __init__(self, raw=..., filtered=..., state=...):
        # 与えた辞書型に変更があった場合は参照先も書き換わることになる
        self.filtered = defaultdict(bool) if filtered == ... else filtered
        self.raw = defaultdict(bool) if raw == ... else raw
        self.state = defaultdict(LightState) if state == ... else state

        self.stop_event = threading.Event()  # 停止させるかのフラグ
        self.previous = defaultdict(bool)  # 直前のON・OFF状態を格納しておく

        self.queue_target = queue.Queue()  # 点灯状態を検出している対象をstrで格納する

        self.lock = threading.Lock()
        # チャタリング除去のスレッドは最初から最後まで回しておく
        self.filter_thread = threading.Thread(target=self.chattering_eliminator, args=(self.lock,))
        self.filter_thread.start()
        # 定期的に実行したい処理
        self.regularly_thread = threading.Thread(target=self.regularly_check)
        self.regularly_thread.start()

    def regularly_check(self):
        '''
        定期的に状態判断を実行させる
        '''
        REGURARLY_INTERVAL = 120
        LOOP_INTERVAL = 1
        time_start = time.perf_counter()  # time.time()より高精度
        while not self.stop_event.is_set():
            if (time.perf_counter() - time_start >= REGURARLY_INTERVAL):
                for k, v in self.filtered.items():
                    self.state_check(k, v)
                # ロギング
                for k, v in self.state.items():
                    info = "{},{},{} ".format(datetime.now().strftime("%Y/%m/%d %H:%M:%S"), k, v)
                    logger.info(info)
                # print('test')
                time_start = time.perf_counter()
            time.sleep(LOOP_INTERVAL)

    def chattering_eliminator(self, lock):
        '''検出結果（self.raw）のチャタリング(フリッカー？)を除去して、フィルタを掛けた結果（self.filtered）を得る'''
        LOOP_INTERVAL = 0.2
        while not self.stop_event.is_set():
            for k, v in self.raw.items():
                if v != self.previous.get(k, False):
                    data = []
                    for _ in range(10):  # 指定回数投票して多数決
                        time.sleep(0.02)
                        data.append(self.raw.get(k, False))
                    ave = sum(data) / len(data)
                    self.previous[k] = True if ave >= 0.5 else False
                    # if v == self.previous[k]:  # 判定結果がvと一致する場合、状態が変わったと判断できる
                    #     self.state_check(k, v) # 変化があった時に状態判断を実行
                    with lock:  # self.filteredは別のスレッドと共有するのでロックしてアクセス
                        self.filtered[k] = self.previous[k]
            time.sleep(LOOP_INTERVAL)

    def monitor(self, name, is_on, queue, lock):
        ''' 点滅・点灯状態をチェックする '''
        queue.put(name)  # モニタリング対象をstrでqueueに格納しておく（これを見てモニタリング中かどうか外部から判断する）
        # print(name, 'monitoreing started')

        MONITOR_TIME = 1.0
        INTERVAL = 0.1
        blinking_state = LightState.ON if is_on else LightState.OFF

        def is_inverted(name, is_on, lock):
            time_start = time.perf_counter()  # time.time()より高精度
            # with lock:
            #     before = self.filtered.get(name, False)
            while (time.perf_counter() - time_start < MONITOR_TIME):
                time.sleep(INTERVAL)
                with lock:
                    after = self.filtered.get(name, False)
                if after != is_on:
                    return True
            return False

        time.sleep(INTERVAL)
        if is_inverted(name, is_on, lock):
            with lock:
                after = self.filtered.get(name, False)
                blinking_state = LightState.ON if after else LightState.OFF
            time.sleep(INTERVAL)
            if is_inverted(name, after, lock):
                with lock:
                    blinking_state = LightState.BLINK

        with lock:
            self.state[name] = blinking_state
            # print(name, 'is', self.state[name])

        _ = queue.get()  # モニタリングが終わった時点でqueueから対象のリストを除外する

    def state_check(self, msg, current):
        # print(msg, 'state chekck')
        if msg in self.queue_target.queue:  # 対象(msg)をモニタリングするスレッドを作っている状態なら何もしない
            return
        # 点灯、点滅状態を確認するスレッドを実行する
        t = threading.Thread(target=self.monitor, args=(msg, current, self.queue_target, self.lock,))
        t.start()

    def stop(self):
        '''スレッドを停止させる'''
        self.stop_event.set()
        self.filter_thread.join()  # スレッドが停止するのを待つ
        self.regularly_thread.join()
        print('thread finished')


class LightDetect(QDialog):
    AREA_THRESHOLD = 700
    # シグナル
    is_detected = pyqtSignal(str, bool)  # 毎フレームごとに各色の検出結果を発生させるシグナル

    def __init__(self, raw=..., filtered=..., state=...):
        '''
        :param raw:
        クラス生成時に辞書型の引数（raw）を与えておくことで、状態変化があった場合に検出結果を反映させることができる
        与える引数はデフォルトディクトにしておいた方が良い
        '''
        # UI初期化処理
        super(LightDetect, self).__init__()
        loadUi('light_detect.ui', self)
        self.init_ui()
        self.image = ...
        self.track_enable = True
        # 与えた辞書型に変更があった場合は参照先も書き換わることになる
        self.raw = defaultdict(bool) if raw == ... else raw
        self.filtered = defaultdict(bool) if filtered == ... else filtered
        self.state = defaultdict(LightState) if state == ... else state

        # 監視用スレッドの開始
        self.monitor = StateMonitor(self.raw, self.filtered, self.state)

        self.set_color1()
        self.set_color2()
        self.set_color3()

    def init_ui(self):
        self.btnStart.clicked.connect(self.start_webcam)
        self.btnStop.clicked.connect(self.stop_webcam)

        self.btnTrack.setCheckable(True)
        self.btnTrack.toggled.connect(self.track_webcam_color)

        self.btnColor1.clicked.connect(self.set_color1)
        self.btnColor2.clicked.connect(self.set_color2)
        self.btnColor3.clicked.connect(self.set_color3)

        self.sldHueMin.valueChanged.connect(self.current_hsv)
        self.sldSatMin.valueChanged.connect(self.current_hsv)
        self.sldValMin.valueChanged.connect(self.current_hsv)
        self.sldHueMax.valueChanged.connect(self.current_hsv)
        self.sldSatMax.valueChanged.connect(self.current_hsv)
        self.sldValMax.valueChanged.connect(self.current_hsv)

        self.is_detected.connect(self.update_detected_state)

    # スロット
    def update_detected_state(self, msg, current):
        previous = self.raw.get(msg, False)
        if previous != current:
            self.raw[msg] = current
            # print(self.raw, is_on_raw)

    def current_hsv(self):
        lower = np.array([self.sldHueMin.value(), self.sldSatMin.value(), self.sldValMin.value()], np.uint8)
        upper = np.array([self.sldHueMax.value(), self.sldSatMax.value(), self.sldValMax.value()], np.uint8)
        self.lblCurrentHSV.setText("Min{0}  Max{1}".format(str(lower), str(upper)))

    def track_webcam_color(self, status):
        if status:
            self.track_enable = True
            self.btnTrack.setText('色検出　停止')
        else:
            self.track_enable = False
            self.btnTrack.setText('色検出')

    def set_color1(self):
        self.color1_lower = np.array([self.sldHueMin.value(), self.sldSatMin.value(), self.sldValMin.value()], np.uint8)
        self.color1_upper = np.array([self.sldHueMax.value(), self.sldSatMax.value(), self.sldValMax.value()], np.uint8)
        self.lblColor1.setText("HSV1: Min{0}  Max{1}".format(str(self.color1_lower), str(self.color1_upper)))

    def set_color2(self):
        self.color2_lower = np.array([self.sldHueMin.value(), self.sldSatMin.value(), self.sldValMin.value()], np.uint8)
        self.color2_upper = np.array([self.sldHueMax.value(), self.sldSatMax.value(), self.sldValMax.value()], np.uint8)
        self.lblColor2.setText("HSV2: Min{0}  Max{1}".format(str(self.color2_lower), str(self.color2_upper)))

    def set_color3(self):
        self.color3_lower = np.array([self.sldHueMin.value(), self.sldSatMin.value(), self.sldValMin.value()], np.uint8)
        self.color3_upper = np.array([self.sldHueMax.value(), self.sldSatMax.value(), self.sldValMax.value()], np.uint8)
        self.lblColor3.setText("HSV3: Min{0}  Max{1}".format(str(self.color3_lower), str(self.color3_upper)))

    def start_webcam(self):
        self.capture = cv2.VideoCapture(0)

        # 露光補正を行う
        if os.name == 'posix':  # Linuxの場合
            # v4l2で設定を行う前に一度read()を実行しておく
            _, _ = self.capture.read()
            # subprocessを用いてv4l2の設定を実行,マニュアル露出にして値を設定
            cmd = 'v4l2-ctl -d /dev/video0 -c exposure_auto=1 -c exposure_absolute=20'
            ret = subprocess.check_output([cmd], shell=True)
            # v4l2の設定値を確認
            cmd = 'v4l2-ctl --list-ctrls'
            ret = subprocess.check_output([cmd], shell=True)
            print(ret)
        elif os.name == 'nt':  # Windowsの場合
            # Logicool c270はこれで設定できる　c615はだめみたい
            self.capture.set(cv2.CAP_PROP_AUTO_EXPOSURE, 0)  # 自動露光解除(win7:0)
            # self.capture.set(cv2.CAP_PROP_EXPOSURE, 0.0)  # 露光補正値（win7:-7.0～0.0くらいまでは確認した）
            self.capture.set(cv2.CAP_PROP_EXPOSURE, -1.0)  # 露光補正値（win7:-7.0～0.0くらいまでは確認した）
            print(self.capture.get(cv2.CAP_PROP_EXPOSURE))
        else:
            print('Unsuported OS. Exposure setting could not be performed.')

        self.capture.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
        self.capture.set(cv2.CAP_PROP_FRAME_WIDTH, 640)

        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_frame)
        # self.timer.start(5)
        self.timer.start(20)

    def hsv_mask(self, img, lower, upper):
        '''
        lower, upperの大小関係が入れ替わった時にはマスク範囲を反転させる
        （特にHueによる色の分布が環状になっているのでこのような範囲指定が必要。Satとvalueはそのままで良いかも？）
        hsv = cv2.cvtColor(self.image, cv2.COLOR_BGR2HSV_FULL)
        などを用いて、H,S,Vともに256段階で保持していることが前提
        :param img:
        :param lower:
        :param upper:
        '''
        h_l, h_u = lower[0], upper[0]
        s_l, s_u = lower[1], upper[1]
        v_l, v_u = lower[2], upper[2]

        h_mask = cv2.inRange(img,
                             np.array([min(h_l, h_u), 0, 0], np.uint8),
                             np.array([max(h_l, h_u), 255, 255], np.uint8))
        s_mask = cv2.inRange(img,
                             np.array([0, min(s_l, s_u), 0], np.uint8),
                             np.array([255, max(s_l, s_u), 255], np.uint8))
        v_mask = cv2.inRange(img,
                             np.array([0, 0, min(v_l, v_u)], np.uint8),
                             np.array([255, 255, max(v_l, v_u)], np.uint8))
        if h_l > h_u:
            h_mask = cv2.bitwise_not(h_mask)  # 領域を反転させる
        if s_l > s_u:
            s_mask = cv2.bitwise_not(s_mask)
        if v_l > v_u:
            v_mask = cv2.bitwise_not(v_mask)

        return h_mask & s_mask & v_mask

    def update_frame(self):
        ret, self.image = self.capture.read()
        self.image = cv2.flip(self.image, 1)
        self.display_image(self.image, 1)

        # COLOR_BGR2HSV_FULLを指定した場合,H,S,Vともに256段階で保持する。
        hsv = cv2.cvtColor(self.image, cv2.COLOR_BGR2HSV_FULL)

        color_lower = np.array([self.sldHueMin.value(), self.sldSatMin.value(), self.sldValMin.value()], np.uint8)
        color_upper = np.array([self.sldHueMax.value(), self.sldSatMax.value(), self.sldValMax.value()], np.uint8)

        color_mask = self.hsv_mask(hsv, color_lower, color_upper)

        self.display_image(color_mask, 2)

        if self.track_enable:
            tracked_image = self.track_colored_object(self.image.copy())
            self.display_image(tracked_image, 1)
        else:
            self.display_image(self.image, 1)

    def track_colored_object(self, img):
        blur = cv2.blur(img, (3, 3))
        # COLOR_BGR2HSV_FULLを指定した場合,H,S,Vともに256段階で保持する。
        hsv = cv2.cvtColor(self.image, cv2.COLOR_BGR2HSV_FULL)

        def drow_detect_box(img, color_lower, color_upper, msg, color):
            '''
            :param img:
            :param color_lower:
            :param color_upper:
            :param msg:
            :param color:tupleで指定 e.g. (0, 0, 255)
            :return:
            '''
            color_mask = self.hsv_mask(hsv, color_lower, color_upper)
            erode = cv2.erode(color_mask, None, iterations=2)
            dilate = cv2.dilate(erode, None, iterations=10)

            kernel_open = np.ones((5, 5))
            kernel_close = np.ones((20, 20))

            mask_open = cv2.morphologyEx(dilate, cv2.MORPH_OPEN, kernel_open)
            mask_close = cv2.morphologyEx(mask_open, cv2.MORPH_CLOSE, kernel_close)

            (_, counters, hierarchy) = cv2.findContours(mask_close, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

            areas = [cv2.contourArea(cnt) for cnt in counters]
            if areas and max(areas) > self.AREA_THRESHOLD:
                self.is_detected.emit(msg, True)
                for cnt in counters:
                    x, y, w, h = cv2.boundingRect(cnt)
                    img = cv2.rectangle(img, (x, y), (x + w, y + h), color, 2)
                    cv2.putText(img, msg, (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 1)
            else:
                self.is_detected.emit(msg, False)

        msg_1 = 'HSV1'
        msg_2 = 'HSV2'
        msg_3 = 'HSV3'
        color_1 = (0, 0, 255)
        color_2 = (0, 255, 0)
        color_3 = (255, 0, 0)

        if self.cbxColor1.isChecked():
            drow_detect_box(img, self.color1_lower, self.color1_upper, msg=msg_1, color=color_1)
        else:
            self.raw[msg_1] = False

        if self.cbxColor2.isChecked():
            drow_detect_box(img, self.color2_lower, self.color2_upper, msg=msg_2, color=color_2)
        else:
            self.raw[msg_2] = False

        if self.cbxColor3.isChecked():
            drow_detect_box(img, self.color3_lower, self.color3_upper, msg=msg_3, color=color_3)
        else:
            self.raw[msg_3] = False

        return img

    def stop_webcam(self):
        self.timer.stop()

    def display_image(self, img, window=1):
        qformat = QImage.Format_Indexed8
        if len(img.shape) == 3:  # rows[0], cols[1], channels[2]
            if img.shape[2] == 4:
                qformat = QImage.Format_RGBA8888
            else:
                qformat = QImage.Format_RGB888
        out_image = QImage(img, img.shape[1], img.shape[0], img.strides[0], qformat)
        # BGR -> RGB
        out_image = out_image.rgbSwapped()

        if window == 1:
            self.lblImage.setPixmap(QPixmap.fromImage(out_image))
            self.lblImage.setScaledContents(True)
        if window == 2:
            self.lblProcessed.setPixmap(QPixmap.fromImage(out_image))
            self.lblProcessed.setScaledContents(True)

    def closeEvent(self, event):
        # スレッドを停止させる
        self.monitor.stop()
        print('close')


if __name__ == "__main__":
    # アプリケーション作成
    app = QApplication(sys.argv)

    # オブジェクト作成
    window = LightDetect(is_on_raw, is_on_filtered, blinking_state)
    window.setWindowTitle('light detect')
    # MainWindowの表示
    window.show()
    # MainWindowの実行
    sys.exit(app.exec_())
